/*******************************************************************************
 * Cody Berlin
 *******************************************************************************/
package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.BinaryOperation;

/**
 * Divide operation
 */
public class Divide extends AbstractOperation implements BinaryOperation {
	
	@Override
	public float perform(float arg1, float arg2) {
		if (arg2 <= (float)0.0) {
			return (float)0.0;
		}
		return arg1 / arg2;
	}

	@Override
	public String getName() {
		return "/";
	}

}
