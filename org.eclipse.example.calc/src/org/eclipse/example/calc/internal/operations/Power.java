/*******************************************************************************
 * Cody Berlin
 *******************************************************************************/
package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.BinaryOperation;
import Math;

/**
 * Power of operation
 */
public class Power extends AbstractOperation implements BinaryOperation {

	@Override
	public float perform(float arg1, float arg2) {
		return (float)Math.pow((double)arg1, (double)arg2);
	}

	@Override
	public String getName() {
		return "^";
	}

}
